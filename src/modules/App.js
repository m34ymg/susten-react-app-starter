import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import styled from "styled-components";
import NavbarComponent from "../components/NavbarComponent";
import Footer from "../components/Footer";

import HomePage from "../pages/HomePage";
import AboutPage from "../pages/AboutPage";
import TopicsPage from "../pages/TopicsPage";
import CardsPage from "../pages/CardsPage";

const ContentHolder = styled.div`
  padding: 15px;
`;

function App() {
  return (
    <Router>
      <NavbarComponent />
      <ContentHolder>
        <Route exact path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/topics" component={TopicsPage} />
        <Route path="/cards" component={CardsPage} />
      </ContentHolder>
      <Footer />
    </Router>
  );
}

export default App;
