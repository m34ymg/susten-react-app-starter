import React from "react";
import { Flex, Box } from "reflexbox";
import RippledCardComponent from "../components/RippledCardComponent";

/* Need to better manage color scheme..  */
const sustenColors = {
  navy: {
    primary: "#355263",
    secondary: "#90adc1"
  },
  red: {
    primary: "#c7414a",
    secondary: "#ffa5a5"
  },
  orange: {
    primary: "#c87c22",
    secondary: "#ffdd81"
  },
  green: {
    primary: "#007961",
    secondary: "#56dbbe"
  },
  blue: {
    primary: "#006b9d",
    secondary: "#5ecaff"
  },
  purple: {
    primary: "#65578d",
    secondary: "#c5b4f0"
  }
};

const colors = Object.keys(sustenColors);
const boxWidth = [1, 1 / 3, 1 / 6];

function CardsPage() {
  return (
    <React.Fragment>
      <h2>Card Grid</h2>

      <Flex wrap>
        {[1, 2, 3].map(level =>
          colors.map(col => (
            <Box w={boxWidth}>
              <RippledCardComponent
                color={col}
                title="Title"
                primary={sustenColors[col].primary}
                secondary="#f0f0f0"
                description="lorem ipsum ipsum ipsum"
              />
            </Box>
          ))
        )}
      </Flex>
    </React.Fragment>
  );
}

export default CardsPage;
