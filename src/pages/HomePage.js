import React from "react";
import { Flex, Box } from "reflexbox";
const colors = ["navy", "red", "orange", "green", "blue", "purple"];

function ColorBox(prop) {
  let className = "bg-" + prop.color + prop.level;
  return (
    <Box
      className={className}
      p={1}
      w={[1, 1 / 6, 1 / 6]}
      style={{
        color: "white",
        height: "100px",
        fontStyle: "italic"
      }}
    >
      {className}
    </Box>
  );
}

function HomePage() {
  return (
    <React.Fragment>
      <h2>Susten Starter App Home</h2>

      <p>Responsive Grid Example</p>
      <Flex wrap>
        {[1, 2, 3].map(i =>
          colors.map(c => <ColorBox key={c} color={c} level={i} />)
        )}
      </Flex>
    </React.Fragment>
  );
}

export default HomePage;
