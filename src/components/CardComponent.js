import React from "react";
import styled from "styled-components";

const Outer = styled.div`
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  //   max-width: 300px;
  height: 200px;
  margin: 5px;
  text-align: center;
`;

const Header = styled.p`
  font-size: 18px;
  color: white;
  padding: 8px;
`;

const Description = styled.p`
  padding: 5px;
  text-align: left;
`;

function CardComponent(props) {
  let level = props.level ? props.level : "1";
  var bgColorClass = "bg-" + props.color + level;

  return (
    <div>
      <Outer>
        <Header className={bgColorClass}>{props.title}</Header>
        <Description>{props.description}</Description>
      </Outer>
    </div>
  );
}

export default CardComponent;
