import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "@blueprintjs/core";

const Outer = styled.div`
  display: block;
  position: relative;
  //   max-width: 262px;
  background-color: ${props => props.secondary};
  border-radius: 4px;
  padding: 32px 24px;
  margin: 12px;
  text-decoration: none;
  z-index: 0;
  overflow: hidden;

  &:before {
    content: "";
    position: absolute;
    z-index: -1;
    top: -16px;
    right: -16px;
    background: ${props => props.primary};
    height: 32px;
    width: 32px;
    border-radius: 32px;
    transform: scale(1);
    transform-origin: 50% 50%;
    transition: transform 0.25s ease-out;
  }

  &:hover:before {
    transform: scale(40);
  }

  :hover {
    .header {
      transition: all 0.3s ease-out;
      color: rgba(255, 255, 255, 0.8);
    }
    .desc {
      transition: all 0.3s ease-out;
      color: rgba(255, 255, 255, 0.8);
    }
  }
`;

const Header = styled.h3`
  color: #262626;
  font-size: 17px;
  line-height: 24px;
  font-weight: 700;
  margin-bottom: 4px;
`;

const Description = styled.p`
  font-weight: 400;
  line-height: 20px;
  color: #666666;
  font-size: 14px;
`;

const Corner = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 32px;
  height: 32px;
  overflow: hidden;
  top: 0;
  right: 0;
  background-color: ${props => props.primary};
  border-radius: 0 4px 0 32px;
`;

const Arrow = styled.div`
  margin-top: -4px;
  margin-right: -4px;
  color: white;
  font-family: courier, sans;
`;

function RippledCardComponent(props) {
  return (
    <div>
      <Outer
        primary={props.primary ? props.primary : "#00838d"}
        secondary={props.secondary ? props.secondary : "#f2f8f9"}
      >
        <Header className="header">{props.title}</Header>
        <Description className="desc">{props.description}</Description>
        <Corner primary={props.primary ? props.primary : "#00838d"}>
          <Arrow>
            <Icon icon="arrow-right" iconSize={15} />
          </Arrow>
        </Corner>
      </Outer>
    </div>
  );
}

export default RippledCardComponent;
