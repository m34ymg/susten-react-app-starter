import React from "react";
import SustenLogo from "../img/susten_logo.svg";
import styled from "styled-components";
import { Link } from "react-router-dom";

import {
  Alignment,
  Classes,
  Navbar,
  NavbarGroup,
  NavbarHeading,
  NavbarDivider,
  Button
} from "@blueprintjs/core";

const CenteredImg = styled.img`
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  width: 30px;
`;

const NavbarOffsetter = styled.p`
  margin: 50px;
`;

const NoUL = { textDecoration: "none", color: "#182026" };

function NavbarLeftMenu() {
  return (
    <React.Fragment>
      <Link to="/">
        <NavbarHeading>
          <p style={{ width: "20px" }}>
            <CenteredImg src={SustenLogo} />
          </p>
        </NavbarHeading>
      </Link>
      <Link to="/" style={NoUL}>
        susten
      </Link>
    </React.Fragment>
  );
}

function NavbarCenterMenu() {
  return (
    <React.Fragment>
      <Link to="/topics" style={NoUL}>
        <Button className={Classes.MINIMAL} icon="layers" text="Topics" />
      </Link>
      <Link to="/cards" style={NoUL}>
        <Button className={Classes.MINIMAL} icon="document" text="Cards" />
      </Link>
      <Link to="/about" style={NoUL}>
        <Button className={Classes.MINIMAL} icon="help" text="About" />
      </Link>
    </React.Fragment>
  );
}

function NavbarRightMenu() {
  return (
    <React.Fragment>
      <Button className={Classes.MINIMAL} icon="user" />
      <Button className={Classes.MINIMAL} icon="notifications" />
      <Button className={Classes.MINIMAL} icon="cog" />
    </React.Fragment>
  );
}
const fixedToTop = true;

function NavbarComponent() {
  return (
    <React.Fragment>
      <Navbar fixedToTop={fixedToTop}>
        <NavbarGroup>
          <NavbarLeftMenu />
        </NavbarGroup>

        <NavbarGroup align={Alignment.LEFT}>
          <NavbarDivider />
          <NavbarCenterMenu />
        </NavbarGroup>

        <NavbarGroup align={Alignment.RIGHT}>
          <NavbarDivider />
          <NavbarRightMenu />
        </NavbarGroup>
      </Navbar>

      {fixedToTop && <NavbarOffsetter />}
    </React.Fragment>
  );
}

export default NavbarComponent;
