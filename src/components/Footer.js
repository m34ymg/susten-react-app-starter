import React from "react";

import styled from "styled-components";

const CenteredFooter = styled.div`
  text-align: center;
  font-size: 10px;
`;

export default function() {
  return (
    <CenteredFooter>
      ©株式会社sustenキャピタル・マネジメント 2019
    </CenteredFooter>
  );
}
